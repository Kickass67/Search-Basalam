package dev.basalam.searchproduct.utils

import java.text.NumberFormat
import java.util.*

class StringFormatter {
    companion object{
        fun getPersianNumber(price: Double): String {
            val nf = NumberFormat.getInstance(Locale("fa", "IR"))
            return nf.format(price)
        }

        fun addVendorTitle(name: String): String {
            return "غرفه: $name"
        }

        fun addWeightUnit(weight: String): String {
            return "$weight گرم"
        }

        fun addPriceUnit(price: String): String {
            return "$price تومان"
        }

        fun arrangeRating(rating: Double, count: Int): String {
            val rating = getPersianNumber(rating)
            val count = getPersianNumber(count.toDouble())
            return "($count)  $rating"
        }
    }
}