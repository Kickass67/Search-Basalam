package dev.basalam.searchproduct.utils

class Constants {

    companion object{
        const val BASE_URL = "https://api.basalam.com/"
        const val SEARCH_REQUEST_QUERY = "{productSearch(size: 20) {products {id name photo(size: LARGE)" +
                "  { url } vendor { name } weight price rating" +
                " { rating count: signals } } } }"
    }
}