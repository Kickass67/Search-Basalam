package dev.basalam.searchproduct.utils

sealed class DataState<out R> {
    data class Success<out T>(val Data: T) : DataState<T>()
    data class Error(val exception: Exception) : DataState<Nothing>()
    object Loading : DataState<Nothing>()
}