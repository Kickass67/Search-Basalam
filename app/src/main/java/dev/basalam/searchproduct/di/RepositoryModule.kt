//package dev.basalam.searchproduct.di
//
//import dagger.Module
//import dagger.Provides
//import dagger.hilt.InstallIn
//import dagger.hilt.android.components.ApplicationComponent
//import dev.basalam.searchproduct.repository.MainRepository
//import dev.basalam.searchproduct.retrofit.ProductRetrofit
//import dev.basalam.searchproduct.room.ProductDao
//import dev.basalam.searchproduct.utils.Constants
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory
//import javax.inject.Singleton
//
//@Module
//@InstallIn(ApplicationComponent::class)
//object RepositoryModule {
//
//    @Singleton
//    @Provides
//    fun provideMainRepository(
//            productDao: ProductDao,
//            productRetrofit: ProductRetrofit
//    ): MainRepository {
//        return MainRepository(productDao, productRetrofit)
//    }
//}
