package dev.basalam.searchproduct.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ActivityRetainedScoped
import dev.basalam.searchproduct.room.ProductDao
import dev.basalam.searchproduct.room.ProductDatabase

@Module
@InstallIn(ActivityRetainedComponent::class)
object RoomModule {

    @Provides
    @ActivityRetainedScoped
    fun provideProductDb(@ApplicationContext context: Context): ProductDatabase {
        return Room.databaseBuilder(
            context,
            ProductDatabase::class.java,
            ProductDatabase.DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @ActivityRetainedScoped
    fun provideProductDao(productDatabase: ProductDatabase): ProductDao {
        return productDatabase.productDao()
    }
}