package dev.basalam.searchproduct.retrofit

import dev.basalam.searchproduct.model.SearchRequestBody
import dev.basalam.searchproduct.model.SearchResponseBody
import retrofit2.http.Body
import retrofit2.http.POST

interface ProductRetrofit {
    @POST("api/user")
    suspend fun getProducts(@Body searchRequestBody: SearchRequestBody): SearchResponseBody
}