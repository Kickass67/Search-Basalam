package dev.basalam.searchproduct


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dev.basalam.searchproduct.repository.MainRepository
import dev.basalam.searchproduct.room.Product
import dev.basalam.searchproduct.utils.DataState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel
@Inject
constructor(
    private val mainRepository: MainRepository,
) : ViewModel() {

    private val _dataState: MutableLiveData<DataState<List<Product>>> = MutableLiveData()
    val dataState: LiveData<DataState<List<Product>>>
        get() = _dataState

    init {
        setStateEvent()
    }

    fun setStateEvent() {
        viewModelScope.launch {

            mainRepository.getProducts().collect {
                when (it) {
                    is DataState.Error -> _dataState.value = DataState.Error(it.exception)
                    DataState.Loading -> _dataState.value = DataState.Loading
                    is DataState.Success -> _dataState.value = DataState.Success(it.Data)
                }

            }
        }
    }
}
