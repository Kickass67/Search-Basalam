package dev.basalam.searchproduct.room

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import dev.basalam.searchproduct.model.Photo
import dev.basalam.searchproduct.model.Rating
import dev.basalam.searchproduct.model.Vendor

@Entity(tableName = "products")
data class Product(
    @PrimaryKey(autoGenerate = false)
    val id: Double,
    val name: String,
    @Embedded
    val photo: Photo,
    @Embedded
    val vendor: Vendor,
    val weight: String,
    val price: Double,
    @Embedded
    val rating: Rating
)
