package dev.basalam.searchproduct.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import dev.basalam.searchproduct.R
import dev.basalam.searchproduct.room.Product
import dev.basalam.searchproduct.utils.StringFormatter
import dev.basalam.searchproduct.utils.StringFormatter.Companion.addPriceUnit
import dev.basalam.searchproduct.utils.StringFormatter.Companion.addVendorTitle
import dev.basalam.searchproduct.utils.StringFormatter.Companion.addWeightUnit
import dev.basalam.searchproduct.utils.StringFormatter.Companion.arrangeRating


class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.MyViewHolder>() {

    private var products: ArrayList<Product> = ArrayList()

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView = itemView.findViewById<View>(R.id.name) as TextView
        val pictureImageView = itemView.findViewById<View>(R.id.productPicture) as ImageView
        val ratingTextView = itemView.findViewById<View>(R.id.rating) as TextView
        val vendorTextView = itemView.findViewById<View>(R.id.vendor) as TextView
        val weightTextView = itemView.findViewById<View>(R.id.weight) as TextView
        val priceTextView = itemView.findViewById<View>(R.id.price) as TextView

        fun bind(product: Product) {
            nameTextView.text = product.name
            vendorTextView.text = addVendorTitle(product.vendor.name)
            weightTextView.text =
                addWeightUnit(StringFormatter.getPersianNumber(product.weight.toDouble()))
            priceTextView.text = addPriceUnit(StringFormatter.getPersianNumber(product.price))
            ratingTextView.text = arrangeRating(product.rating.rating, product.rating.count)
            Picasso.get()
                .load(product.photo.url)
                .placeholder(R.drawable.ic_baseline_image_24)
                .error(R.drawable.ic_baseline_image_24)
                .into(pictureImageView)
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.row_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(products[position])
    }


    fun setData(newProducts: List<Product>) {
        products.addAll(newProducts)
        notifyDataSetChanged()
    }
}

//adapter differ