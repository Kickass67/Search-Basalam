package dev.basalam.searchproduct.repository

import dev.basalam.searchproduct.model.SearchRequestBody
import dev.basalam.searchproduct.retrofit.ProductRetrofit
import dev.basalam.searchproduct.room.Product
import dev.basalam.searchproduct.room.ProductDao
import dev.basalam.searchproduct.utils.Constants.Companion.SEARCH_REQUEST_QUERY
import dev.basalam.searchproduct.utils.DataState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class MainRepository @Inject
constructor(
    private val productDao: ProductDao,
    private val productretrofit: ProductRetrofit
) {
    suspend fun getProducts(): Flow<DataState<List<Product>>> = flow {
        emit(DataState.Loading)

        if (productDao.hasData()) {
            emit(DataState.Success(productDao.getAll()))
        }
//        delay(5000)
        try {
            val retrofitProducts =
                productretrofit.getProducts(SearchRequestBody(SEARCH_REQUEST_QUERY)).data.productSearch.products
            if (retrofitProducts.isNotEmpty()) {
                productDao.deleteAll()
                productDao.insertAll(retrofitProducts)
                emit(DataState.Success(productDao.getAll()))
            }


        } catch (e: Exception) {
            emit(DataState.Error(e))
        }
    }
}
