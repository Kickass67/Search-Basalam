package dev.basalam.searchproduct

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import dev.basalam.searchproduct.adapter.ProductsAdapter
import dev.basalam.searchproduct.room.Product
import dev.basalam.searchproduct.utils.DataState

@AndroidEntryPoint
class SearchFragment : Fragment() {

    lateinit var adapter: ProductsAdapter
    private val viewModel: MainViewModel by viewModels()
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.recyclerView) as RecyclerView
//        val searchButton = view.findViewById(R.id.search) as ImageView
        progressBar = view.findViewById(R.id.loading_progress_bar) as ProgressBar
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        adapter = ProductsAdapter()
        recyclerView.adapter = adapter

        subscribeObserver()

    }

    private fun subscribeObserver() {
        viewModel.dataState.observe(requireActivity(), Observer { dataState ->
            when (dataState) {
                is DataState.Success<List<Product>> -> {
                    displayProgressbar(false)
                    showProducts(dataState.Data)
                }

                is DataState.Error -> {
                    displayProgressbar(false)
                    displayError(dataState.exception.message)
                }

                is DataState.Loading -> {
                    displayProgressbar(true)
                }
            }
        })
    }

    private fun displayError(message: String?) {
        if (message != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } else {
            Toast.makeText(context, "Unknown Error", Toast.LENGTH_LONG).show()
        }
    }

    private fun displayProgressbar(isDisplayed: Boolean) {
        progressBar.visibility = if (isDisplayed) View.VISIBLE else View.GONE
    }

    private fun showProducts(products: List<Product>) {
        adapter.setData(products)
    }

}