package dev.basalam.searchproduct.model

data class SearchRequestBody(
    val query:String
) {
}