package dev.basalam.searchproduct.model

data class Rating(
    val rating: Double,
    val count: Int
) {
}