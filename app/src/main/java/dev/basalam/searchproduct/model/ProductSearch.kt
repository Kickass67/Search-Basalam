package dev.basalam.searchproduct.model

import dev.basalam.searchproduct.room.Product

class ProductSearch(
    val products: List<Product>
)
