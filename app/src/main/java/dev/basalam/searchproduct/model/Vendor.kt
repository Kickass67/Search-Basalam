package dev.basalam.searchproduct.model

import androidx.room.ColumnInfo

data class Vendor(
    @ColumnInfo(name = "VendorName")
    val name: String
) {
}